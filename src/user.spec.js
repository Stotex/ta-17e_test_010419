const user = require("./user");

describe("user", () => {
	it("user id is 1", () => {
		const usr = user(1);
		expect(usr).toMatchSnapshot();
	});
	it("user id is 56", () => {
		const usr = user(56);
		expect(usr).toMatchSnapshot();
	});
	it("user id is 1345", () => {
		const usr = user(1345);
		expect(usr).toMatchSnapshot();
	});
	it("user id is string", () => {
		expect(() => {
			user("abc");
		}).toThrow("id needs to be integer");
	});
});